﻿namespace Unity3dForm
{
    partial class MnUnity
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MnUnity));
            this.axUnityWeb = new AxUnityWebPlayerAXLib.AxUnityWebPlayer();
            this.openFileUnity = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.axUnityWeb)).BeginInit();
            this.SuspendLayout();
            // 
            // axUnityWeb
            // 
            this.axUnityWeb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axUnityWeb.Enabled = true;
            this.axUnityWeb.Location = new System.Drawing.Point(0, 0);
            this.axUnityWeb.Name = "axUnityWeb";
            this.axUnityWeb.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axUnityWeb.OcxState")));
            this.axUnityWeb.Size = new System.Drawing.Size(571, 454);
            this.axUnityWeb.TabIndex = 0;
            // 
            // openFileUnity
            // 
            this.openFileUnity.FileName = "openFileUnity";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(2, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(67, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "选择文件";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MnUnity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 454);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.axUnityWeb);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MnUnity";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "没有文件";
            this.Shown += new System.EventHandler(this.MnUnity_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.axUnityWeb)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxUnityWebPlayerAXLib.AxUnityWebPlayer axUnityWeb;
        private System.Windows.Forms.OpenFileDialog openFileUnity;
        private System.Windows.Forms.Button button1;
    }
}

