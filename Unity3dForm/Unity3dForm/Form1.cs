﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Unity3dForm
{
    public partial class MnUnity : Form
    {
        public Boolean isRun = false;

        AxUnityWebPlayerAXLib.AxUnityWebPlayer axUnityWeb1;

        [DllImport("User32")]  
        public extern static void SetCursorPos(int x, int y);

        public MnUnity()
        {
            InitializeComponent();
        }

        private void MnUnity_Shown(object sender, EventArgs e)
        {
            //MessageBox.Show("是否删除？", "提示", MessageBoxButtons.YesNo);

            this.openFileUnity.Filter = "unity运行文件|*.unity3d";
            this.openFileUnity.Title = "选择运行文件";
            this.openFileUnity.FileName = "";

            this.Controls.Remove(this.axUnityWeb);
            this.axUnityWeb = null;

            this.button1.FlatStyle = FlatStyle.Flat;   //样式  
            this.button1.ForeColor = Color.Transparent;//前景  
            this.button1.BackColor = Color.Transparent;//去背景  
            this.button1.FlatAppearance.BorderSize = 0;//去边线 
            this.button1.FlatAppearance.MouseOverBackColor = Color.FromArgb(50, 40, 60, 82);
            this.button1.FlatAppearance.MouseDownBackColor = Color.FromArgb(50, 40, 60, 82);

            setUnityPlay("");

            //;Point pt = new Point();
            //this.PointToClient(pt);
            //this.PointToScreen(pt);

            //MessageBox.Show(this.Top+"", "提示", MessageBoxButtons.YesNo);

            SetCursorPos(this.Left + 10, this.Top + 40);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            unityCtrl();
        }

        private void unityCtrl()
        {
            if (isRun)
            {
                setUnityPlay("");

                this.button1.Text = "选择文件";

                isRun = false;
            }
            else 
            {
                if (this.openFileUnity.ShowDialog() == DialogResult.OK) 
                {
                    string FileName = this.openFileUnity.FileName;

                    this.Text = this.openFileUnity.SafeFileName;

                    setUnityPlay(FileName);

                    this.button1.Text = "停止运行";

                    if (this.WindowState == FormWindowState.Maximized)
                    {
                        this.WindowState = FormWindowState.Normal;
                    }
                    

                    isRun = true;
                }
            }
        }

        private void setUnityPlay(string fileName) 
        {
            this.Controls.Remove(axUnityWeb1);
            axUnityWeb1 = null;

            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MnUnity));

            // Create a ocx state object with the correct path
            axUnityWeb1 = new AxUnityWebPlayerAXLib.AxUnityWebPlayer();
            ((System.ComponentModel.ISupportInitialize)(axUnityWeb1)).BeginInit();
            axUnityWeb1.OcxState = (AxHost.State)(resources.GetObject("axUnityWeb1.OcxState"));
            axUnityWeb1.TabIndex = 0;
            Controls.Add(axUnityWeb1);
            ((System.ComponentModel.ISupportInitialize)(axUnityWeb1)).EndInit();
            axUnityWeb1.src = fileName;

            AxHost.State state = axUnityWeb1.OcxState;
            axUnityWeb1.Dispose();

            // Create the unity web player object
            axUnityWeb1 = new AxUnityWebPlayerAXLib.AxUnityWebPlayer();
            ((System.ComponentModel.ISupportInitialize)(axUnityWeb1)).BeginInit();
            this.SuspendLayout();
            axUnityWeb1.Dock = System.Windows.Forms.DockStyle.Fill;
            axUnityWeb1.Name = "Unity";
            axUnityWeb1.OcxState = state;
            axUnityWeb1.Size = new System.Drawing.Size(571, 454);
            axUnityWeb1.TabIndex = 0;
            Controls.Add(axUnityWeb1);
            ((System.ComponentModel.ISupportInitialize)(axUnityWeb1)).EndInit();
            this.ResumeLayout(false);
        }
    }
}